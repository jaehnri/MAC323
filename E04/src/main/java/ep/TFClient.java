package ep; /**********************************************************************
 * 
 * $ java-algs4 TFClient < DATA/10.txt > 10.out
 * 
 **********************************************************************/

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class TFClient {

    public static void main(String[] args) {
		In in = new In(args[0]);
		String[] lines = in.readAllLines();
		for (int i = 0; i < lines.length; i++) {
			TokenFinder tf = new TokenFinder(lines[i]);

			StdOut.println(i + ":");
			String token = tf.nextToken();

			int j = 0;
			while (token != null) {
				StdOut.printf("  %3d: %s\n", j++, token);
				token = tf.nextToken();
			}
		}
    }
}
