package ep;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.RedBlackBST;
import edu.princeton.cs.algs4.ST;

public class GeoLocator {

    RedBlackBST<Long, Location> locationByIP;

    private static String[] readTextFromFile(String filename) {
        In in = new In(filename);
        return in.readAllLines();
    }

    public GeoLocator(String filename) {
        locationByIP = new RedBlackBST<Long, Location>();

        String[] rawIPs = readTextFromFile(filename);

        for (String ipAndLocation : rawIPs) {
            TokenFinder tf = new TokenFinder(ipAndLocation);

            Long minIp = IPConv.noip(tf.nextToken());

            // Ignore maxIp
            tf.nextToken();
            String region = tf.nextToken();
            String country = tf.nextToken();
            String state = tf.nextToken();
            String city = tf.nextToken();
            double latitude = Double.parseDouble(tf.nextToken());
            double longitude = Double.parseDouble(tf.nextToken());

            Location location = new Location(region, country, state, city, latitude, longitude);

            locationByIP.put(minIp, location);
        }
    }

    public Location location(String q) {
        return locationByIP.get(locationByIP.floor(IPConv.noip(q)));
    }
}
