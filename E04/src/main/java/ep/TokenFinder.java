package ep;

public class TokenFinder {
    String[] tokens;
    int currentToken = 0;

    private final static String LINE_EXTRACTOR_REGEX = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
    private final static String TRIM_BEGINNING_AND_ENDING_QUOTES_REGEX = "^\"|\"$";

    public TokenFinder(String line) {
        this.tokens = line.split(LINE_EXTRACTOR_REGEX, -1);
    }

    public String nextToken() {
        if (currentToken == tokens.length) {
            return null;
        }
        String token =  this.tokens[currentToken];
        this.currentToken++;

        return token.replaceAll(TRIM_BEGINNING_AND_ENDING_QUOTES_REGEX, "");
    }
}
