/*********************************************************************

 AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP,
 DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA.
 TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO-PROGRAMA (EP) FORAM
 DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE
 EP E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU
 RESPONSÁVEL POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO
 DISTRIBUI OU FACILITEI A SUA DISTRIBUIÇÃO. ESTOU CIENTE DE QUE OS
 CASOS DE PLÁGIO SÃO PUNIDOS COM REPROVAÇÃO DIRETA NA DISCIPLINA.

 NOME: João Henri Carrenho Rocha
 NUSP: 11796378

 Referências: com a exceção de códigos fornecidos no enunciado e em
 aula, caso você tenha utilizado alguma referência, liste-as
 explicitamente para que seu programa não seja considerada plágio.

 - Esse programa foi desenvolvido usando o gerenciador de dependências Gradle. Entretanto, se
 você possuir o CLI do livro de Sadgewick e Wayne instalado, você pode compilar e rodar usando o
 seguinte comando:

 $ java-algs4 -Xss100m KnightOneSuperDeluxe.java [TAMANHO_TABULEIRO_INICIAL] [TAMANHO_TABULEIRO_FINAL]

 Foi setado um timeout de 1 segundo. Para interromper a execucão de um backtracking
 que pode demorar demais, foi utilizada a classe Thread. O algoritmo para tanto foi
 retirado dessa resposta do StackOverflow:

 - https://stackoverflow.com/a/25929223/13038767

 *********************************************************************/

package ep;

import edu.princeton.cs.algs4.IndexMinPQ;
import edu.princeton.cs.algs4.StdOut;


public class KnightOneSuperDeluxe {

    public static class ChessPoint implements Comparable<ChessPoint>{
        private final int x;
        private final int y;
        private int possibilities;

        public ChessPoint(int x, int y, int possibilities) {
            this.x = x;
            this.y = y;
            this.possibilities = possibilities;
        }

        @Override
        public int compareTo(ChessPoint chessPoint) {
            return Integer.compare(this.possibilities, chessPoint.possibilities);
        }
    }

    static int[][] board;
    static int N;
    static boolean end;

    static int[] iMove = {2, 1, -1, -2, -2, -1, 1, 2};
    static int[] jMove = {1, 2, 2, 1, -1, -2, -2, -1};

    /**
     * If true: print board. Otherwise, prints only if there is a solution.
     */
    static boolean shouldPrintBoard;

    private static void initializeBoard() {
        board = new int[N][N];
    }

    private static int fetchPossibilities(int x, int y) {
        int possibilities = 0;

        for (int t = 0; t < 8; t++) {
            int ii = x + iMove[t];
            int jj = y + jMove[t];

            if (valid(ii, jj) && board[ii][jj] == 0) {
                possibilities++;
            }
        }

        return possibilities;
    }

    private static void findTours(int i, int j, int k) {
        if (!valid(i, j) || board[i][j] != 0) return;
        board[i][j] = k;
        if (k == N * N) {
            printResult();
            end = true;
        }

        if (!end) {
            IndexMinPQ<ChessPoint> nextPossibleChessPoints = new IndexMinPQ<>(8);
            for (int t = 0; t < 8; t++) {
                int ii = i + iMove[t];
                int jj = j + jMove[t];
                nextPossibleChessPoints.insert(t, new ChessPoint(ii, jj, fetchPossibilities(ii, jj)));
            }

            while (!nextPossibleChessPoints.isEmpty()) {
                ChessPoint current = nextPossibleChessPoints.minKey();
                findTours(current.x, current.y, k + 1);
                nextPossibleChessPoints.delMin();
            }

            board[i][j] = 0;
        }
    }

    private static Void findTours(int startingI, int startingJ) {
        initializeBoard();
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                findTours(startingI, startingJ, 1);
        return null;
    }

    private static void printResult() {
        if (shouldPrintBoard) {
            printBoard();
        } else {
            printSuccessResult();
        }
    }

    private static void printBoard() {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++)
                StdOut.printf("%2d ", board[i][j]);
            StdOut.println();
        }
    }

    private static void printSuccessResult() {
        StdOut.printf("There is a Knight's tour on an %dx%d board\n", N, N);
    }

    private static boolean valid(int i, int j) {
        return 0 <= i && i < N && 0 <= j && j < N;
    }

    private static void backtrackWithDifferentStartingPoints() throws InterruptedException {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {

                if (!end) {
                    StdOut.printf("(%d, %d) : ", i, j);
                    int finalI = i;
                    int finalJ = j;

                    Thread thread = new Thread(() -> findTours(finalI, finalJ));
                    thread.start();
                    thread.join(1000);

                    if (thread.isAlive()) {
                        thread.stop();
                    }
                }
            }
        }

        if (!end) {
            StdOut.println("Found no tours");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        int startingTable = Integer.parseInt(args[0]);
        int lastTable = Integer.parseInt(args[1]);

        for (int currentTable = startingTable; currentTable <= lastTable; currentTable++) {
            end = false;
            StdOut.printf("%d: ", currentTable);

            N = currentTable;
            backtrackWithDifferentStartingPoints();
        }
    }
}
