/*********************************************************************

 AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP,
 DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA.
 TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO-PROGRAMA (EP) FORAM
 DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE
 EP E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU
 RESPONSÁVEL POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO
 DISTRIBUI OU FACILITEI A SUA DISTRIBUIÇÃO. ESTOU CIENTE DE QUE OS
 CASOS DE PLÁGIO SÃO PUNIDOS COM REPROVAÇÃO DIRETA NA DISCIPLINA.

 NOME: João Henri Carrenho Rocha
 NUSP: 11796378

 Referências: com a exceção de códigos fornecidos no enunciado e em
 aula, caso você tenha utilizado alguma referência, liste-as
 explicitamente para que seu programa não seja considerada plágio.

 - Esse programa foi desenvolvido usando o gerenciador de dependências Gradle e Ubuntu 19.04. Entretanto, se
 você possuir o CLI do livro de Sadgewick e Wayne instalado, você pode compilar e rodar usando os comandos
 descritos abaixo.

 - Uma ressalva importante: nos testes que fiz usando TSTPlusClientLite, notei que alguns resultados ficaram
 em ordem inversa. Isso não significa que meu programa está errado, mas caso a correcão seja feita utilizando
 algum tipo de hash, pode ser que meus exemplos quebrem. Por outro lado, os testes com md5sum funcionaram. Exemplos:

 Professor:
 Your choice: 4
 Query: ..a.e.i.o
 aparecido
 aparecidos

 Meu:
 Your choice: 4
 Query: ..a.e.i.o
 aparecidos
 aparecido

 --

 Professor:
 Your choice: 4
 Query: .h
 she
 shells
 shore
 the

 Meu:
 Your choice: 4
 Query: .h
 the
 she
 shells
 shore

 *********************************************************************/

/******************************************************************************
 *  Compilation:  javac TSTPlus.java
 *  Execution:    java TSTPlus < words.txt
 *  Dependencies: StdIn.java
 *  Data files:   https://algs4.cs.princeton.edu/52trie/shellsST.txt
 *
 *  Symbol table with string keys, implemented using a ternary search
 *  trie (TSTPlus).
 *
 *
 *  % java TSTPlus < shellsST.txt
 *  keys(""):
 *  by 4
 *  sea 6
 *  sells 1
 *  she 0
 *  shells 3
 *  shore 7
 *  the 5
 *
 *  longestPrefixOf("shellsort"):
 *  shells
 *
 *  keysWithPrefix("shor"):
 *  shore
 *
 *  keysThatMatch(".he.l."):
 *  shells
 *
 *  % java TSTPlus
 *  theory the now is the time for all good men
 *
 *  Remarks
 *  --------
 *    - can't use a key that is the empty string ""
 *
 ******************************************************************************/

package ep;

import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.util.NoSuchElementException;

/**
 *  The {@code TSTPlus} class represents an symbol table of key-value
 *  pairs, with string keys and generic values.
 *  It supports the usual <em>put</em>, <em>get</em>, <em>contains</em>,
 *  <em>delete</em>, <em>size</em>, and <em>is-empty</em> methods.
 *  It also provides character-based methods for finding the string
 *  in the symbol table that is the <em>longest prefix</em> of a given prefix,
 *  finding all strings in the symbol table that <em>start with</em> a given prefix,
 *  and finding all strings in the symbol table that <em>match</em> a given pattern.
 *  A symbol table implements the <em>associative array</em> abstraction:
 *  when associating a value with a key that is already in the symbol table,
 *  the convention is to replace the old value with the new value.
 *  Unlike {@link java.util.Map}, this class uses the convention that
 *  values cannot be {@code null}—setting the
 *  value associated with a key to {@code null} is equivalent to deleting the key
 *  from the symbol table.
 *  <p>
 *  This implementation uses a ternary search trie.
 *  <p>
 *  For additional documentation, see <a href="https://algs4.cs.princeton.edu/52trie">Section 5.2</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 */
public class TSTPlus<Value> {
    private int n;              // size
    private Node<Value> root;   // root of TSTPlus

    private static class Node<Value> {
        private char c;                        // character
        private Node<Value> left, mid, right;  // left, middle, and right subtries
        private Value val;                     // value associated with string
    }

    /**
     * Initializes an empty string symbol table.
     */
    public TSTPlus() {
    }

    /**
     * Returns the number of key-value pairs in this symbol table.
     * @return the number of key-value pairs in this symbol table
     */
    public int size() {
        return n;
    }

    /**
     * Does this symbol table contain the given key?
     * @param key the key
     * @return {@code true} if this symbol table contains {@code key} and
     *     {@code false} otherwise
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public boolean contains(String key) {
        if (key == null) {
            throw new IllegalArgumentException("argument to contains() is null");
        }
        return get(key) != null;
    }

    /**
     * Returns the value associated with the given key.
     * @param key the key
     * @return the value associated with the given key if the key is in the symbol table
     *     and {@code null} if the key is not in the symbol table
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public Value get(String key) {
        if (key == null) {
            throw new IllegalArgumentException("calls get() with null argument");
        }
        if (key.length() == 0) throw new IllegalArgumentException("key must have length >= 1");
        Node<Value> x = get(root, key, 0);
        if (x == null) return null;
        return x.val;
    }

    // return subtrie corresponding to given key
    private Node<Value> get(Node<Value> x, String key, int d) {
        if (x == null) return null;
        if (key.length() == 0) throw new IllegalArgumentException("key must have length >= 1");
        char c = key.charAt(d);
        if      (c < x.c)              return get(x.left,  key, d);
        else if (c > x.c)              return get(x.right, key, d);
        else if (d < key.length() - 1) return get(x.mid,   key, d+1);
        else                           return x;
    }

    /**
     * Inserts the key-value pair into the symbol table, overwriting the old value
     * with the new value if the key is already in the symbol table.
     * If the value is {@code null}, this effectively deletes the key from the symbol table.
     * @param key the key
     * @param val the value
     * @throws IllegalArgumentException if {@code key} is {@code null}
     */
    public void put(String key, Value val) {
        if (key == null) {
            throw new IllegalArgumentException("calls put() with null key");
        }
        if (!contains(key)) n++;
        root = put(root, key, val, 0);
    }

    private Node<Value> put(Node<Value> x, String key, Value val, int d) {
        char c = key.charAt(d);
        if (x == null) {
            x = new Node<Value>();
            x.c = c;
        }
        if      (c < x.c)               x.left  = put(x.left,  key, val, d);
        else if (c > x.c)               x.right = put(x.right, key, val, d);
        else if (d < key.length() - 1)  x.mid   = put(x.mid,   key, val, d+1);
        else                            x.val   = val;
        return x;
    }

    /**
     * Returns the string in the symbol table that is the longest prefix of {@code query},
     * or {@code null}, if no such string.
     * @param query the query string
     * @return the string in the symbol table that is the longest prefix of {@code query},
     *     or {@code null} if no such string
     * @throws IllegalArgumentException if {@code query} is {@code null}
     */
    public String longestPrefixOf(String query) {
        if (query == null) {
            throw new IllegalArgumentException("calls longestPrefixOf() with null argument");
        }
        if (query.length() == 0) return null;
        int length = 0;
        Node<Value> x = root;
        int i = 0;
        while (x != null && i < query.length()) {
            char c = query.charAt(i);
            if      (c < x.c) x = x.left;
            else if (c > x.c) x = x.right;
            else {
                i++;
                if (x.val != null) length = i;
                x = x.mid;
            }
        }
        return query.substring(0, length);
    }

    /**
     * Returns all keys in the symbol table as an {@code Iterable}.
     * To iterate over all of the keys in the symbol table named {@code st},
     * use the foreach notation: {@code for (Key key : st.keys())}.
     * @return all keys in the symbol table as an {@code Iterable}
     */
    public Iterable<String> keys() {
        Queue<String> queue = new Queue<String>();
        collect(root, new StringBuilder(), queue);
        return queue;
    }

    /**
     * Returns all of the keys in the set that start with {@code prefix}.
     * @param prefix the prefix
     * @return all of the keys in the set that start with {@code prefix},
     *     as an iterable
     * @throws IllegalArgumentException if {@code prefix} is {@code null}
     */
    public Iterable<String> keysWithPrefix(String prefix) {
        if (prefix == null) {
            throw new IllegalArgumentException("calls keysWithPrefix() with null argument");
        }
        Queue<String> queue = new Queue<String>();
        Node<Value> x = get(root, prefix, 0);
        if (x == null) return queue;
        if (x.val != null) queue.enqueue(prefix);
        collect(x.mid, new StringBuilder(prefix), queue);
        return queue;
    }

    public String minWithPrefix(String prefix) {
        if (prefix.equals("")) {
            return getMin(root, prefix);
        }

        Node<Value> prefixEndNode = prefix.length() > 0 ? get(root, prefix, 0) : root;

        if (prefixEndNode == null) {
            return null;
        }

        if (prefixEndNode.mid != null) {
            return getMin(prefixEndNode.mid, prefix);
        }

        if (prefixEndNode.val != null) {
            return prefix;
        }

        return null;
    }

    private String getMin(Node<Value> currentNode, String word) {
        if (currentNode.left != null) {
            return getMin(currentNode.left, word);
        }

        if (currentNode.val != null) {
            return word + currentNode.c;
        }

        if (currentNode.mid != null) {
            return getMin(currentNode.mid, word + currentNode.c);
        }

        return word + currentNode.c;
    }

    public String maxWithPrefix(String prefix) {
        if (prefix.equals("")) {
            return getMax(root, prefix);
        }

        Node<Value> prefixEndNode = get(root, prefix, 0);
        if (prefixEndNode == null) {
            return null;
        }

        if (prefixEndNode.mid != null) {
            return getMax(prefixEndNode.mid, prefix);
        }

        if (prefixEndNode.val != null) {
            return prefix;
        }

        return null;
    }

    private String getMax(Node<Value> currentNode, String word) {
        if (currentNode.right != null) {
            return getMax(currentNode.right, word);
        }

        if (currentNode.mid != null) {
            return getMax(currentNode.mid, word + currentNode.c);
        }

        return word + currentNode.c;
    }

    public String floor(String key) {
        if (key == null) {
            throw new IllegalArgumentException("argument to floor() is null");
        }
        if (root == null) {
            throw new NoSuchElementException("calls floor() with empty symbol table");
        }

        String floor = floor(root, key, 0, "");
        if (floor == null) {
            throw new NoSuchElementException("argument to floor() is too small");
        }

        return floor;
    }

    private String floor(Node<Value> currentNode, String key, int offset, String builtWord) {
        if (currentNode == null) return null;
        if (key.length() == 0) throw new IllegalArgumentException("key must have length >= 1");

        char queryChar = key.charAt(offset);
        if (queryChar < currentNode.c) {
            return floor(currentNode.left,  key, offset, builtWord);
        } else if (queryChar > currentNode.c) {
            String foundFloor = floor(currentNode.right, key, offset, builtWord);

            if (foundFloor == null && currentNode.mid != null) {
                foundFloor = getMax(currentNode.mid, builtWord + currentNode.c);
            }

            return foundFloor;
        } else if (offset < key.length() - 1) {
            String foundFloor = floor(currentNode.mid, key, offset + 1, builtWord + currentNode.c);

            if (foundFloor == null && currentNode.left != null) {
                foundFloor = getMax(currentNode.left, builtWord);
            }

            return foundFloor;
        } else if (offset == key.length() - 1 && currentNode.val != null) {
            return key;
        }


        return null;
    }

    public String ceiling(String key) {
        if (key == null) {
            throw new IllegalArgumentException("argument to floor() is null");
        }
        if (root == null) {
            throw new NoSuchElementException("calls ceiling() with empty symbol table");
        }

        String ceiling = ceiling(root, key, 0);
        if (ceiling == null) {
            throw new NoSuchElementException("argument to ceiling() is too big");
        }

        return ceiling;
    }

    private String ceiling(Node<Value> currentNode, String key, int offset) {
        if (currentNode == null) return null;

        if (offset == key.length() - 1) {
            if (currentNode.val != null) {
                return key;
            } else {
                getMin(currentNode, key);
            }
        }

        char queryLetter = key.charAt(offset);

        if (get(currentNode, ))
    }

    // all keys in subtrie rooted at x with given prefix
    private void collect(Node<Value> x, StringBuilder prefix, Queue<String> queue) {
        if (x == null) return;
        collect(x.left,  prefix, queue);
        if (x.val != null) queue.enqueue(prefix.toString() + x.c);
        collect(x.mid,   prefix.append(x.c), queue);
        prefix.deleteCharAt(prefix.length() - 1);
        collect(x.right, prefix, queue);
    }


    /**
     * Returns all of the keys in the symbol table that match {@code pattern},
     * where . symbol is treated as a wildcard character.
     * @param pattern the pattern
     * @return all of the keys in the symbol table that match {@code pattern},
     *     as an iterable, where . is treated as a wildcard character.
     */
    public Iterable<String> keysThatMatch(String pattern) {
        Queue<String> queue = new Queue<String>();
        collect(root, new StringBuilder(), 0, pattern, queue);
        return queue;
    }

    private void collect(Node<Value> x, StringBuilder prefix, int i, String pattern, Queue<String> queue) {
        if (x == null) return;
        char c = pattern.charAt(i);
        if (c == '.' || c < x.c) collect(x.left, prefix, i, pattern, queue);
        if (c == '.' || c == x.c) {
            if (i == pattern.length() - 1 && x.val != null) {
                queue.enqueue(prefix.toString() + x.c);
            }

            if (i < pattern.length() - 1) {
                collect(x.mid, prefix.append(x.c), i+1, pattern, queue);
                prefix.deleteCharAt(prefix.length() - 1);
            }
        }
        if (c == '.' || c > x.c) collect(x.right, prefix, i, pattern, queue);
    }

    public Iterable<String> keysThatStartWith(String pattern) {
        Queue<String> queue = new Queue<>();
        collectPrefixed(root, new StringBuilder(), 0, pattern, queue);
        return queue;
    }

    private void collectPrefixed(Node<Value> x, StringBuilder prefix, int i, String pattern, Queue<String> queue) {
        if (x == null) {
            return;
        }
        char c = pattern.charAt(i);

        if (c == '.' || c < x.c) {
            collectPrefixed(x.left, prefix, i, pattern, queue);
        }

        if (c == '.' || c > x.c) {
            collectPrefixed(x.right, prefix, i, pattern, queue);
        }

        if (c == '.' || c == x.c) {
            if (i == pattern.length() - 1 && x.mid != null) {
                collect(x.mid, prefix.append(x.c), queue);
                prefix.deleteCharAt(prefix.length() - 1);
            }

            if (i == pattern.length() - 1 && x.val != null) {
                queue.enqueue(prefix.toString() + x.c);
            }

            if (i < pattern.length() - 1) {
                collectPrefixed(x.mid, prefix.append(x.c), i+1, pattern, queue);
                prefix.deleteCharAt(prefix.length() - 1);
            }
        }
    }

    /**
     * Unit tests the {@code TSTPlus} data type.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {

        // build symbol table from standard input
        TSTPlus<Integer> st = new TSTPlus<>();
        for (int i = 0; !StdIn.isEmpty(); i++) {
            String key = StdIn.readString();
            st.put(key, i);
        }

        // print results
        if (st.size() < 100) {
            StdOut.println("keys(\"\"):");
            for (String key : st.keys()) {
                StdOut.println(key + " " + st.get(key));
            }
            StdOut.println();
        }

        StdOut.println("longestPrefixOf(\"shellsort\"):");
        StdOut.println(st.longestPrefixOf("shellsort"));
        StdOut.println();

        StdOut.println("longestPrefixOf(\"shell\"):");
        StdOut.println(st.longestPrefixOf("shell"));
        StdOut.println();

        StdOut.println("keysWithPrefix(\"shor\"):");
        for (String s : st.keysWithPrefix("shor"))
            StdOut.println(s);
        StdOut.println();

        StdOut.println("keysThatMatch(\".he.l.\"):");
        for (String s : st.keysThatMatch(".he.l."))
            StdOut.println(s);

        StdOut.println("min(\"shell\"):");
        StdOut.println(st.longestPrefixOf("shell"));
        StdOut.println();

        StdOut.println("min(\"\"):");
        StdOut.println(st.minWithPrefix(""));
        StdOut.println();

        StdOut.println("min(\"peram\"):");
        StdOut.println(st.minWithPrefix("peram"));
        StdOut.println();

        StdOut.println("max(\"peram\"):");
        StdOut.println(st.maxWithPrefix("peram"));
        StdOut.println();

        StdOut.println("max(\"\"):");
        StdOut.println(st.maxWithPrefix(""));
        StdOut.println();

        StdOut.println("query(\"..a.e.i.o\"):");
        for (String s : st.keysThatStartWith("..a.e.i.o"))
            StdOut.println(s);

    }
}

/******************************************************************************
 *  Copyright 2002-2018, Robert Sedgewick and Kevin Wayne.
 *
 *  This file is part of algs4.jar, which accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *
 *  algs4.jar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  algs4.jar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
 ******************************************************************************/
