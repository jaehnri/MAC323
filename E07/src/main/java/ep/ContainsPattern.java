/*********************************************************************

 AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP,
 DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA.
 TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO-PROGRAMA (EP) FORAM
 DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE
 EP E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU
 RESPONSÁVEL POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO
 DISTRIBUI OU FACILITEI A SUA DISTRIBUIÇÃO. ESTOU CIENTE DE QUE OS
 CASOS DE PLÁGIO SÃO PUNIDOS COM REPROVAÇÃO DIRETA NA DISCIPLINA.

 NOME: João Henri Carrenho Rocha
 NUSP: 11796378

 Referências: com a exceção de códigos fornecidos no enunciado e em
 aula, caso você tenha utilizado alguma referência, liste-as
 explicitamente para que seu programa não seja considerada plágio.

 - Esse programa foi desenvolvido usando o gerenciador de dependências Gradle e Ubuntu 19.04. Entretanto, se
 você possuir o CLI do livro de Sadgewick e Wayne instalado, você pode compilar e rodar usando o
 seguinte comando:

 $ java-algs4 GeneratorForPatterns 200 5 .6 aeiou 888 | time -p java-algs4 ContainsPattern Pwords.txt | md5sum

 - Utilizei o método substring da classe padrão String para incluir todos os sufixos de uma determinada palavra:
   https://beginnersbook.com/2013/12/java-string-substring-method-example/

 - Usei a valiosa dica de incluir os sufixos na TST dada em:
   https://algs4.cs.princeton.edu/52trie/

 *********************************************************************/

package ep;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

public class ContainsPattern {

    public static void main(String[] args) {
        In in = new In(args[0]);

        // use TST to hold a set of strings; i.e., ignore values
        TSTPlus<Queue<String>> st = new TSTPlus<>();

        while (!in.isEmpty()) {
            String key = in.readString();
            for (int i = 0; i < key.length(); i++) {
                Queue<String> associatedPrefixes = st.get(key.substring(i));
                if (associatedPrefixes == null) {
                    associatedPrefixes = new Queue<>();
                }
                associatedPrefixes.enqueue(key.substring(0, i));
                st.put(key.substring(i), associatedPrefixes);
            }
        }

        Queue<String> patterns = new Queue<>();
        while (!StdIn.isEmpty()) {
            String pattern = StdIn.readString();
            patterns.enqueue(pattern);
        }

        for (String pattern : patterns) {
            TSTPlus<Integer> words = new TSTPlus<>();
            for (String s : st.keysThatStartWith(pattern)) {

                Queue<String> prefixesForKey = st.get(s);
                for (String prefix : prefixesForKey) {
                    words.put(prefix + s, 0);
                }
            }

            StdOut.println("Words that contain " + pattern + " (" + words.size() + ")");
            for (String w : words.keys()) StdOut.println(w);
            StdOut.println("- * - * -");
        }
    }
}
