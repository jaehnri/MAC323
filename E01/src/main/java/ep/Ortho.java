/*********************************************************************

 AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP,
 DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA.
 TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO-PROGRAMA (EP) FORAM
 DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE
 EP E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU
 RESPONSÁVEL POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO
 DISTRIBUI OU FACILITEI A SUA DISTRIBUIÇÃO. ESTOU CIENTE DE QUE OS
 CASOS DE PLÁGIO SÃO PUNIDOS COM REPROVAÇÃO DIRETA NA DISCIPLINA.

 NOME: João Henri Carrenho Rocha
 NUSP: 11796378

 Referências: com a exceção de códigos fornecidos no enunciado e em
 aula, caso você tenha utilizado alguma referência, liste-as
 explicitamente para que seu programa não seja considerada plágio.

 - Esse programa foi desenvolvido usando o gerenciador de dependências Gradle. Entretanto, se
 você possuir o CLI do livro de Sadgewick e Wayne instalado, você pode compilar e rodar usando o
 seguinte comando:
 $ java-algs4 Ortho.java [CAMINHO_DICIONARIO] < [CAMINHO_TEXTO] > [CAMINHO_PALAVRAS_NAO_ENCONTRADAS]

 - Meu programa é baseado no programa BinarySearch.java em
 https://www.ime.usp.br/~yoshi/2021i/mac0323/sandbox/2021.04.13/BinarySearch.java

 - Utilizei o método compareTo da classe padrão String para fazer a comparacão de Strings
 https://www.w3schools.com/java/ref_string_compareto.asp

 *********************************************************************/

package ep;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;

public class Ortho {

	/**
	 * Given a filename, will return an array of all words in the file.
	 *
	 * @return all words in the given file.
	 */
	private static String[] readTextFromFile(String filename) {
		In in = new In(filename);
		return in.readAllStrings();
	}

	/**
	 * Will return an array of words that are in the STD_IN. Most likely combined with the Unix operator "<".
	 *
	 * @return all words in STD_IN.
	 */
	private static String[] readTextFromStdIn() {
		String rawText = StdIn.readAll();
		return rawText.replaceAll("[^A-Za-z \n]", " ").split("\\s+");
	}

	/**
	 * Using binary search, will search the index of the word in the dictionary.
	 *
	 * @return the index of the word, of -1 if its nonexistent.
	 */
	private static int binarySearch(String[] dictionary, String word) {
		int leftDelimiter = 0;
		int rightDelimiter = dictionary.length - 1;

		while (leftDelimiter <= rightDelimiter) {
			int subArrayMiddle = leftDelimiter + (rightDelimiter - leftDelimiter) / 2;
			int compareToResult = word.compareTo(dictionary[subArrayMiddle]);

			if (compareToResult == 0) {
				return subArrayMiddle;
			}

			if (compareToResult > 0) {
				leftDelimiter = subArrayMiddle + 1;
			} else {
				rightDelimiter = subArrayMiddle - 1;
			}
		}

		return -1;
	}

	/**
	 * Checks if the given word appears in the dictionary. Will check the word like it is originally
	 * written and its full lowercase version.
	 *
	 * @return false if neither words appear in the dictionary.
	 */
	private static boolean wordNotInDictionary(String[] dictionary, String word) {
		if (binarySearch(dictionary, word) != -1) {
			return false;
		} else {
			return binarySearch(dictionary ,word.toLowerCase()) == -1;
		}
	}

	/**
	 * This program reads two files. The first one, that should be the dictionary, is the first
	 * and only argument in the argv. The second file is read via StdIn stream. Specifically, this
	 * file should be passed through the Unix redirect "<" operand.
	 *
	 * Then, this program checks which words from the text aren't in the dictionary and prints it to
	 * the STD_OUT, that should be Unix redirected to another file.
	 */
	public static void main(String[] args) {
		String[] textWords = readTextFromStdIn();

		String filename = args[0];
		String[] dictionary = readTextFromFile(filename);

		for (String word : textWords) {
			if (wordNotInDictionary(dictionary, word)) {
				System.out.println(word);
			}
		}
	}
}
