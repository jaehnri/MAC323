/*********************************************************************

 AO PREENCHER ESSE CABEÇALHO COM O MEU NOME E O MEU NÚMERO USP,
 DECLARO QUE SOU O ÚNICO AUTOR E RESPONSÁVEL POR ESSE PROGRAMA.
 TODAS AS PARTES ORIGINAIS DESSE EXERCÍCIO-PROGRAMA (EP) FORAM
 DESENVOLVIDAS E IMPLEMENTADAS POR MIM SEGUINDO AS INSTRUÇÕES DESSE
 EP E QUE PORTANTO NÃO CONSTITUEM PLÁGIO. DECLARO TAMBÉM QUE SOU
 RESPONSÁVEL POR TODAS AS CÓPIAS DESSE PROGRAMA E QUE EU NÃO
 DISTRIBUI OU FACILITEI A SUA DISTRIBUIÇÃO. ESTOU CIENTE DE QUE OS
 CASOS DE PLÁGIO SÃO PUNIDOS COM REPROVAÇÃO DIRETA NA DISCIPLINA.

 NOME: João Henri Carrenho Rocha
 NUSP: 11796378

 Referências: com a exceção de códigos fornecidos no enunciado e em
 aula, caso você tenha utilizado alguma referência, liste-as
 explicitamente para que seu programa não seja considerada plágio.

 - Esse programa foi desenvolvido usando o gerenciador de dependências Gradle. Entretanto, se
 você possuir o CLI do livro de Sadgewick e Wayne instalado, você pode compilar e rodar usando o
 seguinte comando:

 $ java-algs4 RandomPoints.java 20000 323 | time -p java-algs4 Threshold.java

 Caso você queira que seja printado o grafo d-conexo final, adicione qualquer coisa como último parâmetro. Exemplo:

 $ java-algs4 RandomPoints.java 20000 323 | time -p java-algs4 Threshold.java -

 *********************************************************************/
package ep;

import edu.princeton.cs.algs4.Interval1D;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.UF;

@SuppressWarnings("unchecked")
public class Threshold {

    public static class Point2DWithId {
        private final double x;
        private final double y;
        private final int id;

        public Point2DWithId(double x, double y, int id) {
            if (Double.isInfinite(x) || Double.isInfinite(y))
                throw new IllegalArgumentException("Coordinates must be finite");
            if (Double.isNaN(x) || Double.isNaN(y))
                throw new IllegalArgumentException("Coordinates cannot be NaN");
            if (x == 0.0) this.x = 0.0;  // convert -0.0 to +0.0
            else          this.x = x;

            if (y == 0.0) this.y = 0.0;  // convert -0.0 to +0.0
            else          this.y = y;

            this.id = id;
        }

        public double distanceTo(Point2DWithId that) {
            double dx = this.x - that.x;
            double dy = this.y - that.y;
            return Math.sqrt(dx*dx + dy*dy);
        }

        public void drawTo(Point2DWithId that) {
            StdDraw.line(this.x, this.y, that.x, that.y);
        }

        public void draw() {
            StdDraw.point(x, y);
        }
    }

    private static void initializeCanvas() {
        StdDraw.setCanvasSize(800, 800);
        StdDraw.setXscale(0, 1);
        StdDraw.setYscale(0, 1);
        StdDraw.enableDoubleBuffering();
    }

    private static Queue<Point2DWithId>[][] initializeGrid(int rows, int cols) {
        Queue<Point2DWithId>[][] grid = (Queue<Point2DWithId>[][]) new Queue[rows+2][cols+2];
        for (int i = 0; i <= rows+1; i++) {
            for (int j = 0; j <= cols+1; j++) {
                grid[i][j] = new Queue<>();
            }
        }

        return grid;
    }

    private static Queue<Point2DWithId> readPointsFromStdIn() {
        Queue<Point2DWithId> points = new Queue<>();

        int id = 0;
        while (!StdIn.isEmpty()) {
            double x = StdIn.readDouble();
            double y = StdIn.readDouble();
            Point2DWithId point = new Point2DWithId(x, y, id);
            points.enqueue(point);
            id++;
        }

        return points;
    }

    private static int connectPoints(double d, Queue<Point2DWithId> points) {
        int rows = (int) (1.0 / d);    // # rows in grid
        int cols = (int) (1.0 / d);    // # columns in grid

        Queue<Point2DWithId>[][] grid = initializeGrid(rows, cols);

        UF uf = new UF(points.size());

        for (Point2DWithId point : points) {
            int row = 1 + (int) (point.x * rows);
            int col = 1 + (int) (point.y * cols);

            for (int i = row-1; i <= row+1; i++) {
                for (int j = col-1; j <= col+1; j++) {
                    for (Point2DWithId q : grid[i][j]) {
                        if (point.distanceTo(q) <= d) {
                            uf.union(point.id, q.id);
                        }
                    }
                }
            }
            grid[row][col].enqueue(point);
        }

        return uf.count();
    }

    private static void drawLine(Point2DWithId p, Point2DWithId q, Interval1D interval1D) {
        if (interval1D.contains(p.distanceTo(q))) {
            StdDraw.setPenColor(StdDraw.RED);
        }
        StdDraw.setPenRadius(0.002);
        p.drawTo(q);
        StdDraw.setPenRadius(0.005);
        StdDraw.setPenColor(StdDraw.BLACK);
    }

    private static void connectPointsAndDraw(double d, Queue<Point2DWithId> points, Interval1D interval1D) {
        int rows = (int) (1.0 / d);    // # rows in grid
        int cols = (int) (1.0 / d);    // # columns in grid

        Queue<Point2DWithId>[][] grid = initializeGrid(rows, cols);
        UF uf = new UF(points.size());


        for (Point2DWithId point : points) {
            int row = 1 + (int) (point.x * rows);
            int col = 1 + (int) (point.y * cols);

            for (int i = row-1; i <= row+1; i++) {
                for (int j = col-1; j <= col+1; j++) {
                    for (Point2DWithId q : grid[i][j]) {
                        if (point.distanceTo(q) <= d) {
                            drawLine(point, q, interval1D);
                            uf.union(point.id, q.id);
                        }
                    }
                }
            }
            grid[row][col].enqueue(point);
            point.draw();
        }
        StdDraw.show();
    }

    private static Interval1D getIntervalByBisectionMethod(Queue<Point2DWithId> points) {
        boolean isConnected = false;
        final double intervalMaxSize = Math.pow(10, -9);

        double lowLimit = 0;
        double highLimit = Math.sqrt(2);

        while (!isConnected || (highLimit - lowLimit) > intervalMaxSize) {
            double currentDistance = (lowLimit + highLimit)/2;

            int differentConnections = connectPoints(currentDistance, points);
            isConnected = differentConnections == 1;

            if (isConnected) {
                if ((highLimit - lowLimit) < intervalMaxSize) {
                    return new Interval1D(lowLimit, highLimit);
                }
                highLimit = currentDistance;
            } else {
                lowLimit = currentDistance;
            }
        }

        return new Interval1D(lowLimit, highLimit);
    }

    public static void main(String[] args) {
        boolean shouldPrintCanvas = args.length > 0;

        Queue<Point2DWithId> points = readPointsFromStdIn();
        Interval1D interval1D = getIntervalByBisectionMethod(points);

        StdOut.printf("Connectivity threshold in %s\n", interval1D.toString());

        if (shouldPrintCanvas) {
            initializeCanvas();
            connectPointsAndDraw(interval1D.max(), points, interval1D);
            StdDraw.show();
        }
    }
}
