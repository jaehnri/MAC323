/******************************************************************************
 *  Compilation:  javac DepthFirstSearch.java
 *  Execution:    java DepthFirstSearch filename.txt s
 *  Dependencies: Graph.java StdOut.java
 *  Data files:   https://algs4.cs.princeton.edu/41graph/tinyG.txt
 *                https://algs4.cs.princeton.edu/41graph/mediumG.txt
 *
 *  Run depth first search on an undirected graph.
 *  Runs in O(E + V) time.
 *
 *  % java DepthFirstSearch tinyG.txt 0
 *  0 1 2 3 4 5 6
 *  NOT connected
 *
 *  % java DepthFirstSearch tinyG.txt 9
 *  9 10 11 12
 *  NOT connected
 *
 ******************************************************************************/

package ep;

import edu.princeton.cs.algs4.DepthFirstSearch;
import edu.princeton.cs.algs4.Graph;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdOut;

public class DeletionOrder {
    private boolean[] marked;             // marked[v] = is there an s-v path?
    private Queue<Integer> deletionOrder; // the deletion order so the graph remains connected until it is fully disconnected
    private int count;                    // number of vertices connected to s

    public DeletionOrder(Graph G, int s) {
        marked = new boolean[G.V()];
        deletionOrder = new Queue<>();
        validateVertex(s);
        dfsWithDeletionOrder(G, s);
    }

    // depth first search from v
    private void dfsWithDeletionOrder(Graph G, int v) {
        count++;
        marked[v] = true;
        for (int w : G.adj(v)) {
            if (!marked[w]) {
                dfsWithDeletionOrder(G, w);
            }
        }
        deletionOrder.enqueue(v);
    }

    public boolean marked(int v) {
        validateVertex(v);
        return marked[v];
    }

    public Queue<Integer> getDeletionOrder() {
        return deletionOrder;
    }

    /**
     * Returns the number of vertices connected to the source vertex {@code s}.
     * @return the number of vertices connected to the source vertex {@code s}
     */
    public int count() {
        return count;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        int V = marked.length;
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }

    /**
     * Unit tests the {@code DepthFirstSearch} data type.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {
        In in = new In(args[0]);
        Graph G = new Graph(in);
        int s = Integer.parseInt(args[1]);
        DeletionOrder search = new DeletionOrder(G, s);
        for (int v = 0; v < G.V(); v++) {
            if (search.marked(v))
                StdOut.print(v + " ");
        }

        StdOut.println();
        while (!search.deletionOrder.isEmpty()) {
            int x = search.deletionOrder.dequeue();
            StdOut.println(x);
        }
    }
}
